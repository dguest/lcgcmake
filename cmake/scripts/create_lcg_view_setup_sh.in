#---Source this script to setup the complete environment for this LCG view
#   Generated: @date@

#---Get the location this script (thisdir)
SOURCE=${BASH_ARGV[0]}
if [ "x$SOURCE" = "x" ]; then
    SOURCE=${(%):-%N} # for zsh
fi
thisdir=$(cd "$(dirname "${SOURCE}")"; pwd)
# Note: readlink -f is not working on OSX
thisdir=$(python -c "import os,sys; print(os.path.realpath(os.path.expanduser(sys.argv[1])))" ${thisdir})

#  First the compiler
if [ "$COMPILER" != "native" ] && [ -e @compilerlocation@/setup.sh ]; then
   source @compilerlocation@/setup.sh
fi
#  then the rest...
if [ -z "${PATH}" ]; then
   PATH=${thisdir}/bin; export PATH
else
   PATH=${thisdir}/bin:$PATH; export PATH
fi
if [ Darwin = `uname` ]; then
   library_path=DYLD_LIBRARY_PATH
else
   library_path=LD_LIBRARY_PATH
fi
if [ -z $(eval echo \$\{$library_path\}) ]; then
   eval ${library_path}=${thisdir}/lib64:${thisdir}/lib; export ${library_path}
else
   eval ${library_path}=${thisdir}/lib64:${thisdir}/lib:$(eval echo \$\{$library_path\}); export ${library_path}
fi

pyversions=`find ${thisdir}/lib/ -maxdepth 1 -type d | grep "python[0-9\.]*" | xargs -L1 basename`
for version in $pyversions
do
    PY_PATHS=${thisdir}/lib/$version/site-packages:$PY_PATHS
done

if [ -z "${PYTHONPATH}" ]; then
   PYTHONPATH=${thisdir}/lib:$PY_PATHS; export PYTHONPATH
else
   PYTHONPATH=${thisdir}/lib:$PY_PATHS:$PYTHONPATH; export PYTHONPATH
fi
if [ -z "${MANPATH}" ]; then
   MANPATH=${thisdir}/man; export MANPATH
else
   MANPATH=${thisdir}/man:$MANPATH; export MANPATH
fi
if [ -z "${CMAKE_PREFIX_PATH}" ]; then
   CMAKE_PREFIX_PATH=${thisdir}; export CMAKE_PREFIX_PATH
else
   CMAKE_PREFIX_PATH=${thisdir}:$CMAKE_PREFIX_PATH; export CMAKE_PREFIX_PATH
fi
#---then ROOT
if [ -x $thisdir/bin/root ]; then
   ROOTSYS=$(dirname $(dirname $(readlink $thisdir/bin/root))); export ROOTSYS
   if [ -z "${ROOT_INCLUDE_PATH}" ]; then
      ROOT_INCLUDE_PATH=${thisdir}/include; export ROOT_INCLUDE_PATH
   else
      ROOT_INCLUDE_PATH=${thisdir}/include:$ROOT_INCLUDE_PATH; export ROOT_INCLUDE_PATH
   fi
   if [ -z "${JUPYTER_PATH}" ]; then
      JUPYTER_PATH=${thisdir}/etc/notebook; export JUPYTER_PATH
   else
      JUPYTER_PATH=${thisdir}/etc/notebook:$JUPYTER_PATH; export JUPYTER_PATH
   fi
fi
#---then Geant4
if [ -x $thisdir/bin/geant4-config ]; then
   IFS=$'\n'
   for line in `$thisdir/bin/geant4-config --datasets`; do
      unset IFS
      read dataset var value <<< $line;
      export $var=$value
   done
fi
#---then JAVA
if [ Darwin = `uname` -a -x /usr/libexec/java_home ]; then
  JAVA_HOME=`/usr/libexec/java_home`; export JAVA_HOME
elif [ -x $thisdir/bin/java ]; then
  JAVA_HOME=$(dirname $(dirname $(readlink $thisdir/bin/java))); export JAVA_HOME
fi
#---then SPARK
if [ -x $thisdir/bin/pyspark ]; then
  SPARK_HOME=$(dirname $(dirname $(readlink $thisdir/bin/pyspark))); export SPARK_HOME
fi
#---then HBASE
if [ -x $thisdir/bin/hbase ]; then
  HBASE_HOME=$(dirname $(dirname $(readlink $thisdir/bin/hbase))); export HBASE_HOME
fi
#---then PYTHON
if [ -x $thisdir/bin/python ]; then
  PYTHONHOME=$(dirname $(dirname $(readlink $thisdir/bin/python))); export PYTHONHOME
fi
#---then Jupyter
if [ -x $thisdir/bin/jupyter ]; then
   if [ -z "${JUPYTER_PATH}" ]; then
      JUPYTER_PATH=${thisdir}/share/jupyter; export JUPYTER_PATH
   else
      JUPYTER_PATH=${thisdir}/share/jupyter:$JUPYTER_PATH; export JUPYTER_PATH
   fi
fi
#---then R
if [ -x $thisdir/bin/R ]; then
   R_HOME=`echo "cat(Sys.getenv('R_HOME'))" | $thisdir/bin/R --vanilla --slave`; export R_HOME
   if [ -z "${ROOT_INCLUDE_PATH}" ]; then
       ROOT_INCLUDE_PATH=`echo "cat(Sys.getenv('R_INCLUDE_DIR'))" | $thisdir/bin/R --vanilla --slave`
   else
       ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH:`echo "cat(Sys.getenv('R_INCLUDE_DIR'))" | $thisdir/bin/R --vanilla --slave`
   fi
   ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH:`echo "cat(find.package('RInside'))" | $thisdir/bin/R --vanilla --slave`/include
   ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH:`echo "cat(find.package('Rcpp'))" | $thisdir/bin/R --vanilla --slave`/include
   export ROOT_INCLUDE_PATH
fi
#---then Valgrind
if [ -x $thisdir/bin/valgrind ]; then
   VALGRIND_LIB=$thisdir/lib/valgrind; export VALGRIND_LIB
fi
#---then Delphes
if [ -x $thisdir/bin/DelphesHepMC ]; then
   DELPHES_DIR=$(dirname $(dirname $(readlink $thisdir/bin/DelphesHepMC))); export DELPHES_DIR
fi
#---then Pythia8
if [ -f $thisdir/bin/pythia8-config ]; then
   PYTHIA8=$(dirname $(dirname $(readlink $thisdir/bin/pythia8-config))); export PYTHIA8
   PYTHIA8DATA=$PYTHIA8/share/Pythia8/xmldoc; export PYTHIA8DATA
fi
