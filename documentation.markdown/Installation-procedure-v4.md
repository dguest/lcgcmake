This file describes how to install generators to MCGenerators trees:

1. Download new authors tarfile to local source tarfile repository:

    a. Login as sftnight to lcgapp-slc6-physical1 or lcgapp-slc6-physical2;

    b. Download source tarball;

    c. Put the source tarball into EOS `xrdcp -f <tarball> root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources/MCGeneratorsTarFiles/`;

2. Add package definition to requested version of LCGCMT release: `cmake/toolchain/heptools-<release>.cmake`;

3. Try build on local node and run GENSER test for the given package;

4. Commit changes into git;

6. Go to https://phsft-jenkins.cern.ch/job/lcg_release_tar/
   or to the main Jenkins page https://phsft-jenkins.cern.ch/ and select lcg_release_tar.
   This requires permission (now it is done through the GENSER e-group).

7. Login to Jenkins. This requires a Jenkins account, to be created once (ask Patricia).

8. Select "Build with parameters" and build the package requested:


    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG release version (e.g. 87, ...) |
    | TARGET       | name of the top level package (e.g. `rivet-2.3.0`) |
    | BUILD_POLICY | `limited` |
    | LCG_INSTALL_PREFIX | `None` |
    | Combinations | select the necessary compilers for "slc6" or "centos7". Don't even bother building packages for Ubuntu. | 
    | VERSION_MAIN      | LCGCMake branch to use for the build (LCG_<LCG_VERSION>) | 
  
    Keep the rest of the parameters at their default values.

6. To install the packages, use [lcg_generators_install_afs](https://phsft-jenkins.cern.ch/job/lcg_generators_install_afs) and [lcg_generators_install](https://phsft-jenkins.cern.ch/job/lcg_generators_install) jobs with the following parameters:

    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG release version (e.g. 87, ...) |
    | VERSION_MAIN      | LCGCMake branch to use for the build (LCG_<LCG_VERSION>) | 
    | Combinations | select the necessary compilers for "slc6" or "centos7".  | 

    Keep the rest of the parameters at their default values.

7. If the installation is successfull, mail to genser-announce@cern.ch like this:

    > Dear colleagues,
    
    > New Tauola++ version - Tauola++ 1.1.4 has been installed into MCGenerators lcgcmt trees:

    >  \- MCGenerators_lcgcmt65

    >   \- MCGenerators_lcgcmt65a

    >   \- MCGenerators_lcgcmt66

    >   \---

    >   Best regards,

    >   GENSER
